"""
Contain lambda function for upload csv file to s3 bucket
"""
import os
import boto3
from botocore.exceptions import ClientError

S3_CLIENT = boto3.client("s3",
   region_name="us-east-1",
   endpoint_url="http://"+ os.environ["LOCALSTACK_HOSTNAME"] +":4566",
   aws_access_key_id="test",
   aws_secret_access_key="test"
)

def get_string_from_s3_obj(bucket, key, s3_client=S3_CLIENT):
    """
    Get content from s3 bucket object

    Parameters
    ----------
    bucket: str
        name of s3 bucket
    key: str
        name of object in s3 bucket
    s3_client: boto client
        S3 Client

    Return
    ------
    Str
        content of s3 bucket object

    Exception
    ------
        raise an exception
    """
    try:
        obj = s3_client.get_object(Bucket=bucket, Key=key)
        return obj["Body"].read().decode("utf-8")
    except ClientError as e:
        raise e

def checking_obj_in_bucket(bucket, key, s3_client=S3_CLIENT):
    """
    Get content from s3 bucket object

    Parameters
    ----------
    bucket: str
        name of s3 bucket
    key: str
        name of object in s3 bucket
    s3_client: boto client
        S3 Client

    Return
    ------
    Dict
        Respone about info of object in s3 bucket

    Exception
    ------
        raise an exception
    """
    try:
        respone = s3_client.head_object(Bucket=bucket,
            Key=key
        )
        return respone
    except ClientError as e:
        raise e

def put_obj_to_s3_bucket(bucket, key, data, s3_client=S3_CLIENT):
    """
    Get content from s3 bucket object

    Parameters
    ----------
    bucket: str
        name of s3 bucket
    key: str
        name of object in s3 bucket
    data: str
        content of s3 bucket object
    s3_client: boto client
        S3 Client

    Return
    ------
    Dict
        Respone of putting event

    Exception
    ------
        raise an exception
    """
    try:
        respone = s3_client.put_object(Body=data,
            Bucket=bucket,
            Key=key
        )
        return respone
    except ClientError as e:
        raise e

def count_rows(string):
    """
    Count number of row in string

    Parameters
    ----------
    string: str
        string want to count row

    Return
    ------
    Int
        number of row in string
    """
    # first row will be header
    records = -1
    for row in string.split("\n"):
        if row:
            records += 1

    if records < 0:
        return 0
    return records

def lambda_handler(event, context):
    """
    Lambda function for upload object to s3 bucket event

    Parameters
    ----------
    event: dict
        event trigger lambda function

    Return
    ------
    Valid file type cases
        Dict
            respone of upload object
    Invaldi file type cases
        Str
            "Invalid File Type"

    """
    bucket_name = event["Records"][0]["s3"]["bucket"]["name"]
    time = event["Records"][0]["eventTime"]
    file_name = event["Records"][0]["s3"]["object"]["key"]

    if file_name.endswith('.csv'):
        # get object from bucket
        string = get_string_from_s3_obj(bucket_name, file_name)
        records = count_rows(string)
        lambda_text = file_name + "_" + time + "_" + str(records)

        # check object exist or not
        if checking_obj_in_bucket("bucket-lambda-result","upload_result.txt"):
            string = get_string_from_s3_obj("bucket-lambda-result","upload_result.txt")
            string += "\n" + lambda_text
            return put_obj_to_s3_bucket("bucket-lambda-result", "upload_result.txt", string)

        return put_obj_to_s3_bucket("bucket-lambda-result", "upload_result.txt", lambda_text)
    return "Invalid File Type"


