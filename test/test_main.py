"""
Contain unitest functions
"""
import os
from unittest import mock
import pytest
import boto3
from moto import mock_s3
from botocore.exceptions import ClientError

DEFAULT_REGION = "us-east-1"
S3_BUCKET_CSV = "bucket-csv"
os.environ["LOCALSTACK_HOSTNAME"] = "localhost"

from src.main import get_string_from_s3_obj, count_rows, put_obj_to_s3_bucket, lambda_handler, checking_obj_in_bucket

@pytest.fixture
def mock_s3_client():
    """
    Mock s3 client for unnitest

    Yield
    -----
    s3: S3 Client
    """
    with mock_s3():
        s3_client = boto3.client('s3', region_name=DEFAULT_REGION)
        yield s3_client

# test get string
@pytest.mark.parametrize(
    "file_content, file_key",
    [
        (
            "aaaaaaaa\nbbbbbb\ncccccc\n",
            "data.txt"
        ),
        (
            "head\n123456\nasd456,45678\n789\ntest2345\n",
            "data.csv"
        ),
        (
            "head-_/@123",
            "test.pdf"
        ),
        (
            "",
            "test2.csv"
        )
    ]
)

def test_get_string_from_s3_obj(mock_s3_client, file_key, file_content):
    """
    Run unitest for get_string_from_s3_obj function

    Parameters
    ----------
    mock_s3_client: S3 Client
        Mocked S3 Client for unitest
    file_key: str
        Name of object in s3 bucket
    file_content: str
        Content in object
    """
    # set up
    mock_s3_client.create_bucket(Bucket=S3_BUCKET_CSV)
    mock_s3_client.put_object(Key=file_key,
        Body=file_content,
        Bucket=S3_BUCKET_CSV
    )
    # test
    result = get_string_from_s3_obj(S3_BUCKET_CSV, file_key, mock_s3_client)
    assert result == file_content

@pytest.mark.parametrize(
    "bucket, file_key",
    [
        (
            "true_bucket",
            "wrong_key"
        ),
        (
            "wrong_bucket",
            "true_key"
        ),
        (
            "wrong_bucket",
            "wrong_key"
        )
    ]
)

def test_exception_get_string_from_s3_obj(mock_s3_client, file_key, bucket):
    """
    Run unitest for exception of get_string_from_s3_obj function

    Parameters
    ----------
    mock_s3_client: S3 Client
        Mocked S3 Client for unitest
    file_key: str
        Name of object in s3 bucket
    bucket: str
        Name of s3 bucket
    """
    # set up
    mock_s3_client.create_bucket(Bucket=S3_BUCKET_CSV)
    mock_s3_client.put_object(Key="true_key",
        Body="content",
        Bucket=S3_BUCKET_CSV
    )
    # test
    with pytest.raises(ClientError):
        get_string_from_s3_obj(bucket, file_key, mock_s3_client)

# test checking object
@pytest.mark.parametrize(
    "file_key, expected",
    [
        (
            "data.csv",
            dict
        ),
        (
            "data.txt",
            dict
        )
    ]
)

def test_cheking_obj_in_bucket(mock_s3_client, file_key, expected):
    """
    Run unitest for cheking_obj_in_bucket function

    Parameters
    ----------
    mock_s3_client: S3 Client
        Mocked S3 Client for unitest
    file_key: str
        Name of object in s3 bucket
    expected: Type
        Type = Dict if object is in bucket
        Type = None if object is not in bucket
    """
    # set up
    mock_s3_client.create_bucket(Bucket=S3_BUCKET_CSV)
    mock_s3_client.put_object(Key=file_key,
        Body="aaaaaaaa\nbbbbbb\ncccccc\n",
        Bucket=S3_BUCKET_CSV
    )
    result = checking_obj_in_bucket(S3_BUCKET_CSV, file_key, mock_s3_client)
    assert type(result) == expected

@pytest.mark.parametrize(
    "bucket, file_key",
    [
        (
            "true_bucket",
            "wrong_key"
        ),
        (
            "wrong_bucket",
            "true_key"
        ),
        (
            "wrong_bucket",
            "wrong_key"
        )
    ]
)

def test_exception_cheking_obj_in_bucket(mock_s3_client, file_key, bucket):
    """
    Run unitest for exception of cheking_obj_in_bucket function

    Parameters
    ----------
    mock_s3_client: S3 Client
        Mocked S3 Client for unitest
    file_key: str
        Name of object in s3 bucket
    bucket: str
        Name of s3 bucket
    """
    # set up
    mock_s3_client.create_bucket(Bucket=S3_BUCKET_CSV)
    mock_s3_client.put_object(Key="true_key",
        Body="aaaaaaaa\nbbbbbb\ncccccc\n",
        Bucket=S3_BUCKET_CSV
    )
    with pytest.raises(ClientError):
        checking_obj_in_bucket(bucket, file_key, mock_s3_client)

# test put object
@pytest.mark.parametrize(
    "file_content, file_key",
    [
        (
            "aaaaaaaa\nbbbbbb\ncccccc\n",
            "data.txt"
        ),
        (
            "head\n123456\nasd456,45678\n789\ntest2345\n",
            "data.csv"
        ),
        (
            "head-_/@123",
            "test.pdf"
        ),
        (
            "",
            "test2.csv"
        )
    ]
)

def test_put_obj_to_s3_bucket(mock_s3_client, file_key, file_content):
    """
    Run unitest for put_obj_to_s3_bucket function

    Parameters
    ----------
    mock_s3_client: S3 Client
        Mocked S3 Client for unitest
    file_key: str
        Name of object in s3 bucket
    file_content: str
        Content in object
    """
    # set up
    mock_s3_client.create_bucket(Bucket=S3_BUCKET_CSV)
    #test
    response = put_obj_to_s3_bucket(S3_BUCKET_CSV, file_key, file_content, mock_s3_client)
    assert response["ResponseMetadata"]["HTTPStatusCode"] == 200

def test_exception_put_obj_to_s3_bucket(mock_s3_client):
    """
    Run unitest for exception of put_obj_to_s3_bucket function

    Parameters
    ----------
    mock_s3_client: S3 Client
        Mocked S3 Client for unitest
    """
    # set up
    mock_s3_client.create_bucket(Bucket=S3_BUCKET_CSV)
    #test
    with pytest.raises(ClientError):
        put_obj_to_s3_bucket("wrong_bucket", "data.csv", "content", mock_s3_client)

# test count row
@pytest.mark.parametrize(
    "file_content, rows",
    [
        (
            "aaaaaaaa\nbbbbbb\ncccccc\n",
            2
        ),
        (
            "head\n123456\nasd456,45678\n789\ntest2345\n",
            4
        ),
        (
            "head-_/@123",
            0
        ),
        (
            "",
            0
        )
    ]
)

def test_count_rows(file_content, rows):
    """
    Run unitest for count_rows function

    Parameters
    ----------
    file_content: str
        Content in object
    rows: int
        Number of row in file
    """
    assert count_rows(file_content) == rows

# test lambda function
def mocked_get_string(bucket, key):
    """
    Mock function get_string_from_s3_obj function

    Return
    ------
    Str
        Mocked content
    """
    return "aaaaaaaa\nbbbbbb\ncccccc\n"

def mocked_checking_object(bucket, key):
    """
    Mock function for checking_obj_in_bucket function

    Return
    ------
    Bool
        True
    """
    return True

def mocked_put_object(bucket, key, string):
    """
    Mock function for put_obj_to_s3_bucket function

    Return
    ------
    Int
        200: HTTPS Success Status Code
    """
    return 200

def s3_event(file_name):
    """
    Mock function for lambda_handler event

    Return
    ------
    Dict
        Event of lambda function
    """
    return  {
                "Records":[
                    {
                        "eventTime": "2022-06-22T16:36:50.376Z",
                        "s3":{
                            "bucket":{"name": S3_BUCKET_CSV},
                            "object":{"key": file_name}
                        }
                    }
                ]
            }

@mock.patch("src.main.get_string_from_s3_obj", side_effect=mocked_get_string)
@mock.patch("src.main.checking_obj_in_bucket", side_effect=mocked_checking_object)
@mock.patch("src.main.put_obj_to_s3_bucket", side_effect=mocked_put_object)
def test_valid_lambda_handler(mock_get_string, mock_checking_obj, mock_put_object):
    """
    Run unitest for lambda_handler function
    Valid cases

    Parameters
    ----------
    mock_get_string: function
        Mocked function for get_string_from_s3_obj function
    mock_cheking_obj: function
        Mocked function for checking_obj_in_bucket function
    mock_put_object: function
        Mocked function for put_obj_to_s3_bucket function
    """
    response = lambda_handler(s3_event("data.csv"), "")
    assert response == 200

@mock.patch("src.main.get_string_from_s3_obj", side_effect=mocked_get_string)
@mock.patch("src.main.checking_obj_in_bucket", side_effect=mocked_checking_object)
@mock.patch("src.main.put_obj_to_s3_bucket", side_effect=mocked_put_object)
def test_invalid_lambda_handler(mock_get_string, mock_checking_obj, mock_put_object):
    """
    Run unitest for lambda_handler function
    Invalid cases

    Parameters
    ----------
    mock_get_string: function
        Mocked function for get_string_from_s3_obj function
    mock_cheking_obj: function
        Mocked function for checking_obj_in_bucket function
    mock_put_object: function
        Mocked function for put_obj_to_s3_bucket function
    """
    response = lambda_handler(s3_event("data.txt"), "")
    assert response == "Invalid File Type"


